" This must be first, because it changes other options as a side effect.
set nocompatible    " Use Vim settings (rather then Vi settings)

"  Security
" ----------
set secure
set noexrc

" Getting an E487?
"	- open file
"	- :set ff=unix  (fileformat (mac,dos,unix))
"	- :w
" TODO:
" * resizing split windows after window size change

call plug#begin('~/.vim/vimplugplugins')

    "  Generic
    " ---------
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }	" Install fzf commandline
    Plug 'junegunn/fzf.vim'							" FuzzyfileFinder
    "Plug 'flazz/vim-colorschemes'					" Colorscheme pack, use with fzf's :Color to browse through options
    Plug 'KurtPreston/vimcolors'					" Colorscheme pack, use with fzf's :Color to browse through options
    Plug 'scrooloose/nerdtree' 						" Tree browser
    Plug 'yegappan/mru'							    " Most recently used file browser
    Plug 'itchyny/lightline.vim'                    " Have a good looking status bar
    Plug 'matze/vim-move'                           " Move line/selected text
    Plug 'mhinz/vim-startify'                       " Begin VIM with a start page, showing recently opened files.
    Plug 'MattesGroeger/vim-bookmarks'              " Quick and easy bookmarking
    Plug 'ntpeters/vim-better-whitespace'           " Highlight EOL whitespaces

    "  GIT
    " -----
    Plug 'airblade/vim-gitgutter'				    " GIT indicators in the buffer gutter
    Plug 'tpope/vim-fugitive'						" GIT support
    Plug 'Xuyuanp/nerdtree-git-plugin'              " GIT indicator for NERDtree

    "  Generic Coding
    " ----------------
    Plug 'ludovicchabant/vim-gutentags'				" Automatically update ctags
    Plug 'majutsushi/tagbar'					    " Show ctags overview
    Plug 'vim-syntastic/syntastic'                  " Linter (supports mypy, flake8, ...)
    Plug 'scrooloose/nerdcommenter'				    " Code commenter
    Plug 'junegunn/rainbow_parentheses.vim'         " Show matching characters, with matching colors
    Plug 'ycm-core/YouCompleteMe'                   " Code completion

    "  Specific Coding
    " -----------------
    Plug 'peterhoeg/vim-qml'                        " QML syntax highlighting

    "  Work In Progress
    " ------------------
    Plug 'CoenSchalkwijk/vim-log-highlighting', { 'frozen': 1}  " My personal log highlighting, in dev.

    "  TRY/EXPLORE/REMEMBER
    " ----------------------
    "Plug 'wfxr/minimap.vim'
    Plug 'psf/black'

    Plug 'junegunn/goyo.vim'
    "Plug 'folke/twilight.nvim'
    "Plug 'camspiers/lens.vim'
    " Sourcery
    "Plug 'neoclide/coc.nvim'

    "Plug 'mg979/vim-visual-multi'
    "Plug 'terryma/vim-multiple-cursors'

"    Plug 'sudormrfbin/cheatsheet.nvim'
    "Plug 'nvim-lua/popup.nvim'
    "Plug 'nvim-lua/plenary.nvim'
    "Plug 'nvim-telescope/telescope.nvim'

    " ---
    "Plug 'mbbill/undotree'

    " ---

    "Plug 'nvim-lua/popup.nvim'
    "Plug 'nvim-lua/plenary.nvim'
    "Plug 'nvim-telescope/telescope.nvim'
    "Plug 'pwntester/octo.nvim'
    " ---

    "Plug 'Pocco81/TrueZen.nvim'
    "Plug 'phaazon/hop.nvim'
    "Plug 'ryanoasis/vim-devicons'
    "Plug 'sakhnik/nvim-gdb', { 'do': ':!./install.sh' }
    "Plug 'puremourning/vimspector'

    "Plug 'voldikss/vim-floaterm'
    "Plug 'chubin/cheat.sh'
    "Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
    "Plug 'BurntSushi/ripgrep'
    "Plug 'sharkdp/fd'
    "Plug 'nvim-lua/plenary.nvim'
    "Plug 'nvim-telescope/telescope.nvim'
    "Plug 'vim-vdebug/vdebug'
    "Plug 'jreybert/vimagit'
    "Plug 'ruanyl/vim-gh-line'
    "Plug 'chiel92/vim-autoformat'
    "Plug 'easymotion/vim-easymotion'
    "Plug 'ervandew/supertab'
    "Plug 'terryma/vim-multiple-cursors'
    "Plug 'junegunn/gv.vim'
    "Plug 'inkarkat/vim-mark'
    "Plug 'inkarkat/vim-ingo-library'
    "Plug 'sheerun/vim-polyglot'
    " ...

call plug#end()

"  Startup
" ---------
if &diff
	" Automatically starting Tagbar and/or NERDTree is not always handy,
    " think of using vimdiff for example:
    " $>git difftool --tool=vimdiff
    set diffopt=horizontal      " When using nVim for Git diff; use horizontal separator
else
    " No diff? Do this:
    autocmd VimEnter * NERDTree " When opening VIM, open the NERDTree plugin
    autocmd TabNew * NERDTree   " When opening a new TAB, open the NERDTree plugin

    autocmd Filetype nerdtree set winfixwidth " Sets the NERDTree plugin window width to be a fixed value
    "autocmd VimEnter * Tagbar   " When opening VIM, open the Tagbar plugin
    "autocmd TabNew * Tagbar     " When opening a new TAB, open the Tagbar plugin
endif

function! MyWinEnter()
    set cursorline		" Show a visual line under the cursor's current line.
    set cursorcolumn	" Show a visual line at the cursor's current column.
endfunction

function! MyWinLeave()
    set nocursorline    " Do NOT show a visual line under the cursor's current line.
    set nocursorcolumn  " Do NOT show a visual line at the cursor's current column.
endfunction

"  Hop
" -----
"lua require'hop'.setup()
"noremap <Leader>hw :HopWord<CR>

"  MRU
" -----
"nnoremap <leader>s :mksession!<CR>      " save current session (opened files), reopen session with $vim -S
" Show list of Most Recently Used files
nnoremap <leader>m :MRU<CR>

" Requires the +clipboard feature flag
" Check with :echo has('clipboard') if the output is 0, it's not available
if has('clipboard')
    noremap <Leader>y "+y
    noremap <Leader>p "+p
endif

autocmd VimEnter * wincmd l		        " Jump one window to the right (happens to be the main window)
autocmd TabNew * wincmd l		        " Jump one window to the right (happens to be the main window)

autocmd VimEnter * RainbowParentheses	" Show colored parenthesis

" Make the active window more apparent
autocmd WinEnter * :call MyWinEnter()
autocmd WinLeave * :call MyWinLeave()

"  Generic
" ---------
" It clears the search buffer when you press ',/'
nmap <silent> ,/ :nohlsearch<CR>

let test#strategy = "neovim"

" Change cursor shape in different modes
let &t_EI = "\033[2 q" " NORMAL  █
let &t_SI = "\033[5 q" " INSERT  |
let &t_SR = "\033[3 q" " REPLACE _

"  Terminal
" ----------
let g:floaterm_title = '($1/$2)'        " Title of the floating window
let g:floaterm_height = 0.8             " 0-1 Height of window in % of full height
let g:floaterm_widt = 0.8               " 0-1 Width of windo in % of full width
let g:floaterm_keymap_new = '<F7>'      " Create new terminal
let g:floaterm_keymap_prev = '<F8>'     " Open or move to previous terminal windo
let g:floaterm_keymap_next = '<F9>'     " Open or move to next terminal window
let g:floaterm_keymap_toggle = '<F12>'  " Open or close current terminal window

"  Moving text
" ------
" Move selected block of text up, down, left or right
" Move current line up or down
let g:move_map_keys = 0
vmap <C-down> <Plug>MoveBlockDown
vmap <C-up> <Plug>MoveBlockUp
nmap <C-down> <Plug>MoveLineDown
nmap <C-up> <Plug>MoveLineUp

"  Bookmarks
" -----------
let g:bookmark_display_annotation = 1

"  Startify
" ----------
let g:startify_bookmarks = [ {'a': '~/.vimrc'}, {'b': '~/.config/nvim/init.vim'}, {'c': '~/notebook'} ]
let g:startify_fortune_use_unicode = 1
let g:startify_custom_header = startify#pad(split(system('fortune | cowsay -e Oo -f tux'), '\n'))
let g:startify_change_to_dir = 0  " To not change to the folder of a selected file
let g:startify_fortune_use_unicode = 1

"  Fugitive
" ----------
nnoremap <leader>d :Gdiffsplit<CR>

"  Sourcery
" ----------
"nnoremap <leader>ss :call CocAction('doHover')<CR>
"nnoremap <leader>sf :CocFix<CR>

"  GitGutter
" -----------
set signcolumn=yes                  " Always show the GIT gutter
autocmd BufWritePost * GitGutter    " Force update on save. _should_ not be necessary :/
let g:gitgutter_preview_win_location = 'below'
let g:gitgutter_close_preview_on_escape = 1

"  Rainbow parentheses
" ---------------------
let g:rainbow#max_level = 16
let g:rainbow#pairs = [['(', ')'], ['{', '}'], ['[', ']']]

"  Lightline
" -----------
let g:lightline = {
    \ 'component_function': {
    \   'readonly': 'LightlineReadonly',
    \	'gitbranch': 'FugitiveHead',
    \   'fugitive': 'LightlineFugitive'
\ } }

let g:lightline.active = {
    \ 'left': [ [ 'mode', 'paste' ],
    \           [ 'readonly', 'modified', 'fugitive', 'absolutepath'] ],
    \ 'right': [ [ 'lineinfo' ],
    \            [ 'fileformat', 'filetype', 'fileencoding' ] ] }

let g:lightline.inactive = {
    \ 'left': [ [ '', '' ],
    \           [ '', 'filename', 'modified', 'gitbranch'] ],
    \ 'right': [ [ 'lineinfo' ],
    \            [ 'fileformat', 'filetype', 'fileencoding' ] ] }

" Following requires a NERD FONT (https://www.nerdfonts.com/#home)
let g:lightline.separator = { 'left': '', 'right': '' }
let g:lightline.subseparator = { 'left':'', 'right':'' }

function! LightlineReadonly()
	return &readonly ? '' : ''
endfunction

function! LightlineFugitive()
    if exists('*FugitiveHead')
        let branch = FugitiveHead()
        return branch !=# '' ? ''.branch : ''
    endif
    return ''
endfunction

"  vim-plug
" ----------
let g:plug_window = "new"		" Open in a new buffer (split top)

"  CTag/tagbar/gutentags
" -----------------------
" Regen CTAGS manually with $ctags -R -f .tags.
set tags=./.git/tags/tags;                      " Set CTags file folder
let g:tagbar_width = 40                         " Sets tagbar column width
let g:gutentags_ctags_tagfile = 'tags'
let g:gitroot = substitute(system('git rev-parse --show-toplevel'), '[\n\r]', '', 'g')
let g:gutentags_ctags_exclude=['_build_*']      " Ignore these folders when generating CTags

if g:gitroot !=# ''
	let g:gutentags_cache_dir = g:gitroot .'/.git/tags'
else
	let g:gutentags_cache_dir = $HOME .'/.cache/guten_tags'
endif

"  Goyo
" ------
map <leader>g :Goyo<cr>
let g:goyo_width = 80 " (default: 80)
let g:goyo_height = 85 " (default: 90%)
let g:goyo_linenr = 1 " (default: 0)

function! s:goyo_enter()
  execute 'NERDTreeClose'
  set noshowcmd
  set noshowmode
endfunction

function! s:goyo_leave()
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

"  NERDTree
" ----------
"  Please note the autocmd's for this plugin
" Set NERDTree focus to file in buffer (\r)
map <leader>r :NERDTreeFind<cr>
let NERDTreeShowHidden = 1		    " Show hidden files by default
let NERDTreeWinSize = 40 		    " Resize NERD tree browser (wider)
let NERDTreeIgnore = [
    \ '^\.git$',
    \ '^\..*_cache$',
    \ '^__pycache__$',
    \ '^_build_*'
\ ]   " Do not display these folders in the tree

"  FuzzyFileFinder (fzf)
"  To let Ag/Fzf ignore .gitignore entries, use:
"  export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'
"-----------------------
let g:fzf_layout = { 'down': '40%' }
" Show the buffer browser (\b)
map <leader>b :Buffers<cr>
" Show the search buffer  (\f)
map <leader>f :Files<cr>
" Toggle showing the filebrowser (\n)
map <leader>n :NERDTreeToggle<CR>
"  Black
" -------
map <leader>l :Black<cr>
let g:black_linelength=120

"  Syntastic
"  ----------
let syntastic_style_error_symbol = "?"
let g:syntastic_warning_symbol = "!"
let g:syntastic_error_symbol = "X"
let g:syntastic_python_checkers = ['mypy', 'bandit', 'pylint', 'flake8']    " Check python with these tools
let g:syntastic_aggregate_errors = 1                    " Show all messages at once, not one-by-one
let g:syntastic_always_populate_loc_list = 1            " Always use the location_list to show results
let g:syntastic_auto_loc_list = 1                       " Automatically open the location_list with the results
let g:syntastic_mode_map = {"mode": "passive"}          " Do not automatically perform checks
let g:syntastic_check_on_wq = 0                         " Do not check on write-quits
let g:syntastic_python_flake8_args='--config ~/qa/.flake8 --radon-max-cc 10'        " Use this configuration for python files, when checking with flake8
let g:syntastic_python_mypy_args='--config-file ~/qa/mypy.ini --cache-dir /dev/null' " Use this configuration for python files, when checking with mypy
let g:syntastic_python_pylint_args='--rcfile=~/qa/.pylintrc'  " Overlap with Flake8
let g:syntastic_enable_highlighting=1
" Perform configured checks on the current file (\t)
map <leader>t :SyntasticCheck<cr>

"  Generic VIM settings
" ------------------
" HOWTO insert unicode chars:
" in INSERT/CMD mode: ^V u nnnn
" e.g. ^V u 21B5 for newline char

set nobackup                " Do not use/create back up files
set noswapfile              " Do not use/create .swp files
set hidden                  " Do not unload inactive buffers
set title			        " Shows open file and path in console bar.
set showcmd			        " Display incomplete commands.
set wildmenu                " Enhanced command completion. (default on <tab>).
set autowrite               " Automatically write a file when leaving a modified buffer.
set autoread                " Automatically reread when file is changed from outside
set history=1000            " Keep more commands in history, default: 50.
set lazyredraw              " Only redraw when you have to
set confirm                 " Confirm actions when trying to close VIm with changed buffers
set spelllang=en,cjk        " Set spellcheck language compatibility to Enlish and CJK (Chinese, Japanese, Korean)
set spellsuggest=best,9     " Give the best nine suggestions

"  Indenting
" -----------
set shiftwidth=0	        " Number of spaces to use for indentation. 0 means use tabstop value
set tabstop=4 		        " Number of spaces per TAB
set softtabstop=4           " Set tabs to have 4 spaces. When editing (!)
set expandtab		        " Use spaces instead of tabs.
set smarttab		        " Smart handling of tabs/spaces and indentation.
set backspace=2             " make backspace work like most other programs.

"  Editing UI
" ------------
set nolist
"set list 			" Show hidden characters (hurts the eyes when on by default and are copied to clipboard)
"set listchars=tab:▹\ ,nbsp:␣,trail:◃,eol:↵,precedes:«,extends:»     " Specify how to display hidden characters
set encoding=UTF-8          " Necessary to show Unicode glyphs
"syntax enable               " Enable syntax highlighting while maintaining color settings.

"  UI elements
" -------------
set termguicolors	                    " Enable true color in terminal
set background=dark                     " Setting dark UI colorscheme mode
colorscheme gruvbox                     " Set color scheme to GruvBox
set fillchars+=vert:▕,fold:─,diff:\     " Set vertical split bar character
set laststatus=2                        " Always show the status line..
set number 			                    " Show linenumbers.
set cursorline		                    " Show a visual line under the cursor's current line.
set cursorcolumn                        " Show a visual line at the cursor's current column.
set noshowmode                          " Not showing editing mode; lightline already does that now.
set signcolumn=auto:2                   " Automatically shows (and increases size) of the sign column

"  Searching
" -----------
set incsearch               " Search as you type.
set ignorecase              " Ignore case when searching.
set smartcase               " If search pattern contains uppercase characters, use case sensitive search.
set hlsearch		        " Highlight search matches.

"  Scrolling and wrapping
" ------------------------
set nostartofline           " don't jump to the start of a line when scrolling
set scrolloff=5             " minimum lines to keep above and below cursor
set sidescrolloff=5         " minimum columns to keep above and below cursor

"  Statusline
" ------------
set statusline=%f					                " Filename
set statusline+=\ [%{strlen(&fenc)?&fenc:'none'},	" File encoding
set statusline+=%{&ff}]					            " File format
set statusline+=\ %m					            " Modified flag
"set statusline+=\ %{FugitiveStatusline()}		    " GIT repo
set statusline+=%=					                " Left/right separator
set statusline+=%c,					                " Cursor column
set statusline+=%l/%L					            " Cursor line/total lines
