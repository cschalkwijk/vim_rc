" This must be first, because it changes other options as a side effect.
set nocompatible    " Use Vim settings (rather then Vi settings)

"  Security
" ----------
set secure
set noexrc

" Getting a E487?
"	- open file
"	- :set ff=unix  (fileformat (mac,dos,unix))
"	- :w

call plug#begin('~/.vim/vimplugplugins')

    " TODO:
    " * set working directory: issues with Ag starting from a subfolder
    " * resizing split windows after window size change
    " * flake8 only for current file, not all (probably because of use of
    " '_args'
    " * auto break line when typing too long lines in vimrc... le weird.

    " Generic VIM
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }	" Install fzf commandline
    Plug 'junegunn/fzf.vim'							" FuzzyfileFinder
    Plug 'flazz/vim-colorschemes'					" Colorscheme pack, use with fzf's :Color to browse through options
    Plug 'scrooloose/nerdtree' 						" Tree browser
    Plug 'yegappan/mru'							    " Most recently used file browser
    Plug 'itchyny/lightline.vim'                    " Have a good looking status bar
    Plug 'matze/vim-move'                           " Move line/selected text
    Plug 'mhinz/vim-startify'                       " Begin VIM with a start page, showing recently opened files.
    Plug 'MattesGroeger/vim-bookmarks'              " Quick and easy bookmarking

    " GIT specific
    Plug 'airblade/vim-gitgutter'				    " GIT indicators in the buffer gutter
    Plug 'tpope/vim-fugitive'						" GIT support
    Plug 'tpope/vim-rhubarb'  " derp new
    Plug 'Xuyuanp/nerdtree-git-plugin'              " GIT indicator for NERDtree

    " Generic coding
    Plug 'ludovicchabant/vim-gutentags'				" Automatically update ctags
    Plug 'vim-syntastic/syntastic'                  " Linter (supports mypy, flake8)
    Plug 'scrooloose/nerdcommenter'				    " Code commenter
    Plug 'junegunn/rainbow_parentheses.vim'         " Show matchin characters, with matching colors
    Plug 'ycm-core/YouCompleteMe'                   " Code completion
    Plug 'majutsushi/tagbar'					    " Show ctags overview

    " Language specific
    Plug 'peterhoeg/vim-qml'                        " QML syntax highlighting

    " Work in progress
    Plug 'CoenSchalkwijk/vim-log-highlighting', { 'frozen': 1}  " My personal log highlighting, in dev.

    " TRY/EXPLORE/REMEMBER
    "Plug 'ntpeters/vim-better-whitespace'
    "Plug 'jreybert/vimagit'
    "Plug 'dense-analysis/ale'
    "Plug 'ruanyl/vim-gh-line'
    "Plug 'easymotion/vim-easymotion'
    "Plug 'simnalamburt/vim-mundo'
    "Plug 'ervandew/supertab'
    "Plug 'MattesGroeger/vim-bookmarks'
    "Plug 'kshenoy/vim-signature'
    "Plug 'ryanoasis/vim-devicons'
    "Plug 'terryma/vim-multiple-cursors'
    "Plug 'junegunn/gv.vim'
    "Plug 'inkarkat/vim-mark'
    "Plug 'inkarkat/vim-ingo-library'
    "Plug 'sheerun/vim-polyglot'
    "Plug 'itchyny/calendar.vim'
    " ...

call plug#end()

set diffopt=horizontal

"  Startup
" ---------
if &diff
	" Automatically starting Tagbar and/or NERDTree is not always handy,
    " think of using vimdiff for example:
    " $>git difftool --tool=vimdiff
    set diffopt=horizontal
else
    " No diff? Do this:
    autocmd VimEnter * NERDTree " When opening VIM, open the NERDTree plugin
    autocmd TabNew * NERDTree   " When opening a new TAB, open the NERDTree plugin

    autocmd Filetype nerdtree set winfixwidth " Sets the NERDTree plugin window width to be a fixed value
    "autocmd VimEnter * Tagbar   " When opening VIM, open the Tagbar plugin
    "autocmd TabNew * Tagbar     " When opening a new TAB, open the Tagbar plugin
endif

function! MyWinEnter()
    set cursorline		" Show a visual line under the cursor's current line.
    set cursorcolumn	" Show a visual line at the cursor's current column.
endfunction

function! MyWinLeave()
    set nocursorline    " Do NOT show a visual line under the cursor's current line.
    set nocursorcolumn  " Do NOT show a visual line at the cursor's current column.
endfunction

"  MRU
" -----
"nnoremap <leader>s :mksession!<CR>      " save current session (opened files), reopen session with $vim -S 
nnoremap <leader>M :MRU<CR>             " Show list of Most Recently Used files 
let MRU_Use_Current_Window = 1          " Show MRU overview in existing current window 

" Requires the +clipboard feature flag
" Check with :echo has('clipboard') if the output is 0, it's not available 
if has('clipboard')
    noremap <Leader>y "+y
    noremap <Leader>p "+p
endif

autocmd VimEnter * wincmd l		        " Jump one window to the right (happens to be the main window)
autocmd TabNew * wincmd l		        " Jump one window to the right (happens to be the main window)

autocmd VimEnter * RainbowParentheses	" Show colored parenthesis

" Make the active window more apparent
autocmd WinEnter * :call MyWinEnter()
autocmd WinLeave * :call MyWinLeave()

"  Generic
" ---------
nmap <silent> ,/ :nohlsearch<CR>   " It clears the search buffer when you press ',/'

" Change cursor shape in different modes
let &t_EI = "\033[2 q" " NORMAL  █
let &t_SI = "\033[5 q" " INSERT  |
let &t_SR = "\033[3 q" " REPLACE _

"  Moving text
" ------
" Move selected block of text up, down, left or right
" Move current line up or down
let g:move_map_keys = 0
vmap <C-down> <Plug>MoveBlockDown
vmap <C-up> <Plug>MoveBlockUp
nmap <C-down> <Plug>MoveLineDown
nmap <C-up> <Plug>MoveLineUp

"  Startify
" ----------
let g:startify_bookmarks = [ {'a': '~/.vimrc'}, {'b': '~/notebook'} ]
let g:startify_fortune_use_unicode = 1
let g:startify_custom_header = startify#pad(split(system('fortune | cowsay -e Oo -f tux'), '\n'))
let g:startify_change_to_dir = 0  " To not change to the folder of a selected file
let g:startify_fortune_use_unicode = 1

"  Fugitive
" ----------
nnoremap <leader>d :Gdiffsplit<CR>

"  GitGutter
" ----------- 
set signcolumn=yes                  " Always show the GIT gutter
autocmd BufWritePost * GitGutter    " Force update on save. _should_ not be necessary :/
let g:gitgutter_preview_win_location = 'below'
let g:gitgutter_close_preview_on_escape = 1
"nmap ]c <Plug>GitGutterNextHunk \| :GitGutterPreviewHunk
"nmap [c <Plug>GitGutterPrevHunk \| :GitGutterPreviewHunk

"  Rainbow parentheses
" ---------------------
let g:rainbow#max_level = 16
let g:rainbow#pairs = [['(', ')'], ['{', '}'], ['[', ']']]

"  Lightline
" -----------
let g:lightline = {
    \ 'component_function': {
    \	'gitbranch': 'fugitive#head',
    \   'readonly': 'LightlineReadonly',
	\   'fugitive': 'LightlineFugitive'
\ } }

let g:lightline.active = {
    \ 'left': [ [ 'mode', 'paste' ],
    \           [ 'readonly', 'modified', 'fugitive', 'absolutepath'] ],
    \ 'right': [ [ 'lineinfo' ],
    \            [ 'fileformat', 'filetype', 'fileencoding' ] ] }

let g:lightline.inactive = {
    \ 'left': [ [ '', '' ],
    \           [ '', 'filename', 'modified', 'gitbranch'] ],
    \ 'right': [ [ 'lineinfo' ],
    \            [ 'fileformat', 'filetype', 'fileencoding' ] ] }

" Following requires a NERD FONT (https://www.nerdfonts.com/#home)
let g:lightline.separator = { 'left': '', 'right': '' }
let g:lightline.subseparator = { 'left':'', 'right':'' }

function! LightlineReadonly()
	return &readonly ? '' : ''
endfunction

function! LightlineFugitive()
    if exists('*fugitive#head')
	    let branch = fugitive#head()
	    return branch !=# '' ? ''.branch : ''
	endif
	return ''
endfunction

"  vim-plug
" ----------
let g:plug_window = "new"		" Open in a new buffer (split top)

"  CTag/tagbar/gutentags
" -----------------------
" Regen CTAGS manually with $ctags -R -f .tags.
set tags=./.git/tags/tags;                      " Set CTags file folder
let g:tagbar_width = 40                         " Sets tagbar column width
let g:gutentags_ctags_tagfile = 'tags'
let g:gitroot = substitute(system('git rev-parse --show-toplevel'), '[\n\r]', '', 'g')
let g:gutentags_ctags_exclude=['_build_*']      " Ignore these folders when generating CTags

if g:gitroot !=# ''
	let g:gutentags_cache_dir = g:gitroot .'/.git/tags'
else
	let g:gutentags_cache_dir = $HOME .'/.cache/guten_tags'
endif

"  NERDTree
" ----------
"  Please note the autocmd's for this plugin
map <leader>r :NERDTreeFind<cr>		" Set NERDTree focus to file in buffer (\r)
let NERDTreeShowHidden = 1		    " Show hidden files by default
let NERDTreeWinSize = 40 		    " Resize NERD tree browser (wider)
let NERDTreeIgnore = [
    \ '^\.git$',
    \ '^\..*_cache$',
    \ '^__pycache__$',
    \ '^_build_*'
\ ]   " Do not display these folders in the tree

"  FuzzyFileFinder (fzf)
"  To let Ag/Fzf ignore .gitignore entries, use:
"  export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'
"-----------------------
let g:fzf_layout = { 'down': '40%' }
" Show the buffer browser (\b)
map <leader>b :Buffers<cr>
" Show the search buffer  (\f)
map <leader>f :Files<cr>

"  Syntastic
"  ------------
let syntastic_style_error_symbol = "?"
let g:syntastic_warning_symbol = "!"
let g:syntastic_error_symbol = "X"
"let g:syntastic_python_python_exec = 'python3'
let g:syntastic_python_checkers = ['mypy', 'bandit', 'pylint', 'flake8']    " Check python with mypy and flake8
let g:syntastic_aggregate_errors = 1                    " Show all messages at once, not one-by-one
let g:syntastic_always_populate_loc_list = 1            " Always use the location_list to show results
let g:syntastic_auto_loc_list = 1                       " Automatically open the location_list with the results
let g:syntastic_mode_map = {"mode": "passive"}          " Do not automatically perform checks
let g:syntastic_check_on_wq = 0                         " Do not check on write-quits
let g:syntastic_python_flake8_args='--config ~/qa/.flake8 --radon-max-cc 10'        " Use this configuration for python files, when checking with flake8
let g:syntastic_python_pylint_args='--rcfile ~/qa/.pylintrc'  " Overlap with Flake8
let g:syntastic_python_mypy_args='--config-file ~/qa/mypy.ini'   " Use this configuration for python files, when checking with mypy
let g:syntastic_enable_highlighting=1
map <leader>t :SyntasticCheck<cr>		                " Perform configured checks on the current file (\t)

"  Generic VIM settings
" ------------------
" HOWTO insert unicode chars:
" in INSERT/CMD mode: ^V u nnnn
" e.g. ^V u 21B5 for newline char

set nobackup                " Do not use/create back up files
set noswapfile              " Do not use/create .swp files
set hidden                  " Do not unload inactive buffers
set title			        " Shows open file and path in console bar.
set showcmd			        " Display incomplete commands.
set wildmenu                " Enhanced command completion. (default on <tab>).
set autowrite               " Automatically write a file when leaving a modified buffer.
set autoread                " Automatically reread when file is changed from outside
set history=1000            " Keep more commands in history, default: 50.
set lazyredraw              " Only redraw when you have to
"set sessionoptions=buffers  " When storing a session, store open buffers
set confirm                 " Confirm actions when trying to close VIm with changed buffers

"  Indenting
" -----------
set shiftwidth=0	        " Number of spaces to use for indentation. 0 means use tabstop value
set tabstop=4 		        " Number of spaces per TAB
set softtabstop=4           " Set tabs to have 4 spaces. When editing (!)
set expandtab		        " Use spaces instead of tabs.
set smarttab		        " Smart handling of tabs/spaces and indentation.
set backspace=2             " make backspace work like most other programs.

"  Editing UI
" ------------
set nolist
"set list 			" Show hidden characters (hurts the eyes when on by default and are copied to clipboard)
"set listchars=tab:▹\ ,nbsp:␣,trail:◃,eol:↵,precedes:«,extends:»     " Specify how to display hidden characters
set encoding=UTF-8          " Necessary to show Unicode glyphs
syntax enable               " Enable syntax highlighting while maintaining color settings.

set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=1

"  UI elements
" ------------- 
set termguicolors	                    " Enable true color in terminal
set background=dark                     " Setting dark UI colorscheme mode
colorscheme gruvbox                     " Set color scheme to GruvBox
set fillchars+=vert:▕,fold:─,diff:\     " Set vertical split bar character
set laststatus=2                        " Always show the status line..
set number 			                    " Show linenumbers.
set cursorline		                    " Show a visual line under the cursor's current line.
set cursorcolumn                        " Show a visual line at the cursor's current column.
set noshowmode                          " Not showing editing mode; lightline already does that now.

"  Searching
" -----------
set incsearch               " Search as you type.
set ignorecase              " Ignore case when searching.
set smartcase               " If search pattern contains uppercase characters, use case sensitive search.
set hlsearch		        " Highlight search matches.

"  Scrolling and wrapping
" ------------------------
set nostartofline           " don't jump to the start of a line when scrolling
set scrolloff=5             " minimum lines to keep above and below cursor
set sidescrolloff=5         " minimum columns to keep above and below cursor

"  Statusline
" ------------
set statusline=%f					                " Filename
set statusline+=\ [%{strlen(&fenc)?&fenc:'none'},	" File encoding
set statusline+=%{&ff}]					            " File format
set statusline+=\ %m					            " Modified flag
set statusline+=\ %{FugitiveStatusline()}		    " GIT repo
set statusline+=%=					                " Left/right separator
set statusline+=%c,					                " Cursor column
set statusline+=%l/%L					            " Cursor line/total lines
